var word4 = 'wow JavaScript is so cool';
var exampleFirstWord = word4.substring(0, 3);
var secondWord = word4.substring(4, 14); // do your own!
var thirdWord = word4.substring(15, 17); // do your own!
var fourthWord = word4.substring(18, 20); // do your own!
var fifthWord = word4.substring(21, 25); // do your own!

var firstWordLength = exampleFirstWord.length;
var secondWordLength = secondWord.length;
var thirdWordLength = thirdWord.length;
var fourthWordLength = fourthWord.length;
var fifthWordLength = fifthWord.length;
// create new variables around here

console.log('First Word: ' + exampleFirstWord + ', with length: ' + firstWordLength);
console.log('Second Word: ' + secondWord + ', with length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord + ', with length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord + ', with length: ' + fourthWordLength);
console.log('Fifth Word: ' + fifthWord + ', with length: ' + fifthWordLength);
