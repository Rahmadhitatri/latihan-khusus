var word3 = 'wow JavaScript is so cool';
var exampleFirstWord = word3.substring(0, 3);
var secondWord = word3.substring(4, 14); // do your own!
var thirdWord = word3.substring(15, 17); // do your own!
var fourthWord = word3.substring(18, 20); // do your own!
var fifthWord = word3.substring(21, 25); // do your own!

console.log('First Word: ' + exampleFirstWord);
console.log('Second Word: ' + secondWord);
console.log('Third Word: ' + thirdWord);
console.log('Fourth Word: ' + fourthWord);
console.log('Fifth Word: ' + fifthWord);